package com.harrisonbrock.msscbrewery.services;

import com.harrisonbrock.msscbrewery.web.model.CustomerDto;

import java.util.UUID;

public interface CustomerService {
    CustomerDto getCustomerById(UUID customerId);

    CustomerDto createCustomer(CustomerDto newCustomer);

    void updateCustomerById(UUID customerId, CustomerDto updatedCustomer);

    void deleteCustomerById(UUID customerId);
}
