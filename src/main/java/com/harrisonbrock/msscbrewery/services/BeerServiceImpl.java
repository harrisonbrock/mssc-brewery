package com.harrisonbrock.msscbrewery.services;

import com.harrisonbrock.msscbrewery.web.model.BeerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class BeerServiceImpl implements BeerService {
    @Override
    public BeerDto getBeerById(UUID beerId) {
        return BeerDto.builder().id(UUID.randomUUID())
                .beerName("Galaxy Cat")
                .beerStyle("Pala Ale")
                .build();
    }

    @Override
    public BeerDto createBeer(BeerDto newBeer) {

        return BeerDto.builder()
                .id(UUID.randomUUID())
                .build();
    }

    @Override
    public void updateBeerById(UUID beerId, BeerDto beerDto) {
        // todo
    }

    @Override
    public void deleteById(UUID beerId) {
        // todo
        log.debug("Deleting a beer");
    }
}
