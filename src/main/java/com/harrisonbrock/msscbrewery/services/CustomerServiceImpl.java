package com.harrisonbrock.msscbrewery.services;

import com.harrisonbrock.msscbrewery.web.model.CustomerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {
    @Override
    public CustomerDto getCustomerById(UUID customerId) {
        return CustomerDto.builder()
                .id(UUID.randomUUID())
                .customerName("John")
                .build();
    }

    @Override
    public CustomerDto createCustomer(CustomerDto newCustomer) {
        return CustomerDto.builder()
                .id(UUID.randomUUID())
                .build();
    }

    @Override
    public void updateCustomerById(UUID customerId, CustomerDto updatedCustomer) {
        // todo
    }

    @Override
    public void deleteCustomerById(UUID customerId) {
        // todo
        log.debug("Deleting a customer");
    }
}
