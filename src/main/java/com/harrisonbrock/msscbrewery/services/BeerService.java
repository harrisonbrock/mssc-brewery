package com.harrisonbrock.msscbrewery.services;

import com.harrisonbrock.msscbrewery.web.model.BeerDto;

import java.util.UUID;

public interface BeerService {
    BeerDto getBeerById(UUID beerId);

    BeerDto createBeer(BeerDto newBeer);

    void updateBeerById(UUID beerId, BeerDto beerDto);

    void deleteById(UUID beerId);
}
