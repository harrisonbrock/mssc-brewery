package com.harrisonbrock.msscbrewery.web.mappers;

import com.harrisonbrock.msscbrewery.domain.Customer;
import com.harrisonbrock.msscbrewery.web.model.CustomerDto;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {

    CustomerDto customerToCustomerDto(Customer customer);

    Customer customerDtoToCustomer(CustomerDto dto);
}
