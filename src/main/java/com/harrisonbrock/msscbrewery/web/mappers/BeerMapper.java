package com.harrisonbrock.msscbrewery.web.mappers;

import com.harrisonbrock.msscbrewery.domain.Beer;
import com.harrisonbrock.msscbrewery.web.model.BeerDto;
import org.mapstruct.Mapper;

@Mapper(uses = {DateMapper.class})
public interface BeerMapper {
    BeerDto beerToBeerDto(Beer beer);

    Beer beerDtoToBeer(BeerDto dto);
}
